# 2023-anthology

### Euforia Biru

```
euforia biru
jalan ke utara
dan jadwal di timur tengah

tak pernah ku duga
rasa nasi goreng yang biasa saja
sekumpulan wajah baru yang entah cerita apa

dan tatapannya jelang selesai acara
membuatku tak bisa melupakan
eksplorasi bintang, suara, api
```

### Deru Anak Baru
```
udara hangat pagi
tempat parkir luas
paving block diselingi sedikit rumput

deru gerombolan anak baru
dengan segala semangat awal
kegembiraan bertemu keluarga

dan kembali ke jalan panjang bebas pejalan kaki
dengan tujuan Bandung kembali
Jakarta siang yang padat dan tidak pernah sepi

tanpa skill menyetir di kiri depan
ngobrol ringan dan mamah bapak
posisi duduk sesantai santainya

notifikasi lebah
perasaan berdebar merebak
tentang gadis yang memberiku perasaan kuat
wajah cantik dan tatapan yang amat serasa
```

